package edu.emory.it.services.tki.provider;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AWSSecurityTokenServiceException;
import com.amazonaws.services.securitytoken.model.AssumeRoleWithSAMLRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleWithSAMLResult;
import com.amazonaws.services.securitytoken.model.ExpiredTokenException;
import edu.emory.it.services.tki.util.DuoSecurityUtil;
import edu.emory.it.services.tki.util.ErrorUtil;
import edu.emory.moa.jmsobjects.authentication.v1_0.RoleAssumption;
import edu.emory.moa.jmsobjects.authentication.v1_0.SecurityAssertion;
import edu.emory.moa.objects.resources.v1_0.AssumedRoleUser;
import edu.emory.moa.objects.resources.v1_0.Credential;
import edu.emory.moa.objects.resources.v1_0.Credentials;
import edu.emory.moa.objects.resources.v1_0.RoleAssumptionRequisition;
import edu.emory.moa.objects.resources.v1_0.SecurityAssertionQuerySpecification;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.objects.resources.Error;

import javax.jms.JMSException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

/**
 * Provider for Role Assumption requests.
 * Finishes the login via Duo then assumes an AWS role.
 */
public class AwsRoleAssumptionProvider implements RoleAssumptionProvider {
    private static final Logger logger = LogManager.getLogger("edu.emory.it.services.tki");
    private static final String LOGTAG = "[AwsRoleAssumptionProvider] ";

    private static final String AWS_CREDENTIALS_ACCESS_KEY_PROPERTY = "accessKeyId";
    private static final String AWS_CREDENTIALS_SECRET_KEY_PROPERTY = "secretKey";

    private AppConfig appConfig;
    private Properties duoProperties;
    private ProducerPool tkiServiceProducerPool;

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        if (aConfig == null)
            throw new ProviderException("Must have an AppConfig");

        appConfig = aConfig;

        Properties generalProperties;

        try {
            generalProperties = appConfig.getProperties("GeneralProperties");
            duoProperties = DuoSecurityUtil.getDuoProperties(generalProperties);
        }
        catch (EnterpriseConfigurationObjectException e) {
            throw new ProviderException(e.getMessage());
        }

        // used to send messages to our own service for persistence
        tkiServiceProducerPool = ProviderUtil.getProducerPool("TkiServiceRequestProducer", appConfig, LOGTAG);
    }

    @Override
    public RoleAssumption generate(RoleAssumptionRequisition requisition, List<Error> errors) throws ProviderException {
        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin generate");

        String principal = requisition.getPrincipal();
        String region = requisition.getRegion();
        String principalArn = requisition.getRoleDetail().getPrincipalArn();
        String roleArn = requisition.getRoleDetail().getRoleArn();
        Credential credential = requisition.getCredential();
        Integer durationSeconds = Integer.valueOf(requisition.getDurationSeconds());

        DateFormat df = new SimpleDateFormat("EEE MMM d kk:mm:ss z yyyy");

        // retrieve the SAML response/assertion that was stored by the Security Assertion provider
        // if it's not there, a ProviderException is thrown because the client may be doing something wrong
        // then (hard) delete it as it's no longer needed
        SecurityAssertion securityAssertion = retrieveSecurityAssertion(requisition.getPrincipal());
        final String samlAssertion = securityAssertion.getSamlAssertion();
        deleteSecurityAssertion(securityAssertion);

        /*
         * the AWS credentials are set in the AWSSecurityTokenService client but they don't seem to be
         * used and we speculate it is because we are calling the AssumeRoleWithSAML API.  Evidence that
         * the credentials are not used is the usage report in AWS IAM console showing the key is never
         * accessed.  so, the access/secret key is optional in case we find they are needed under
         * some circumstances.
         */
        AWSCredentialsProvider awsCredentials;
        RoleAssumption roleAssumption;
        try {
            roleAssumption = (RoleAssumption) appConfig.getObjectByType(RoleAssumption.class.getName());

            // when using Duo, the first step is to authorize with Duo
            duoAuth(roleAssumption, principal, credential, errors);
            if (!errors.isEmpty())
                return null;

            // then use the AWS credentials if they're available
            Properties generalProperties = appConfig.getProperties("GeneralProperties");
            String accessKeyId = generalProperties.getProperty(AWS_CREDENTIALS_ACCESS_KEY_PROPERTY);
            String secretKey = generalProperties.getProperty(AWS_CREDENTIALS_SECRET_KEY_PROPERTY);

            if (accessKeyId == null || secretKey == null) {
                awsCredentials = null;
            }
            else {
                awsCredentials = new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretKey));
            }
        }
        catch (EnterpriseConfigurationObjectException | EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }

        AWSSecurityTokenService client = AWSSecurityTokenServiceClientBuilder.standard()
                .withCredentials(awsCredentials)
                .withRegion(region)
                .build();

        AssumeRoleWithSAMLRequest assumeRoleWithSAMLRequest = new AssumeRoleWithSAMLRequest()
                // The base-64 encoded SAML authentication response provided by the IdP
                .withSAMLAssertion(samlAssertion)
                // The Amazon Resource Name (ARN) of the SAML provider in IAM that describes the IdP
                .withPrincipalArn(principalArn)
                // The Amazon Resource Name (ARN) of the role that the caller is assuming
                .withRoleArn(roleArn)
                // The duration, in seconds, of the role session
                .withDurationSeconds(durationSeconds);

        logger.info(LOGTAG + "Assuming role with principalArn=" + principalArn + " roleArn=" + roleArn);

        try {
            AssumeRoleWithSAMLResult assumeRoleWithSAMLResult = client.assumeRoleWithSAML(assumeRoleWithSAMLRequest);

            roleAssumption.setAudience(assumeRoleWithSAMLResult.getAudience());
            roleAssumption.setIssuer(assumeRoleWithSAMLResult.getIssuer());
            roleAssumption.setNameQualifier(assumeRoleWithSAMLResult.getNameQualifier());
            roleAssumption.setSubject(assumeRoleWithSAMLResult.getSubject());
            roleAssumption.setSubjectType(assumeRoleWithSAMLResult.getSubjectType());

            AssumedRoleUser assumedRoleUser = roleAssumption.newAssumedRoleUser();
            assumedRoleUser.setArn(assumeRoleWithSAMLResult.getAssumedRoleUser().getArn());
            assumedRoleUser.setAssumedRoleId(assumeRoleWithSAMLResult.getAssumedRoleUser().getAssumedRoleId());
            roleAssumption.setAssumedRoleUser(assumedRoleUser);

            Credentials raCredentials = roleAssumption.newCredentials();
            raCredentials.setAccessKeyId(assumeRoleWithSAMLResult.getCredentials().getAccessKeyId());
            raCredentials.setExpiration(df.format(assumeRoleWithSAMLResult.getCredentials().getExpiration()));
            raCredentials.setSecretAccessKey(assumeRoleWithSAMLResult.getCredentials().getSecretAccessKey());
            raCredentials.setSessionToken(assumeRoleWithSAMLResult.getCredentials().getSessionToken());
            roleAssumption.setCredentials(raCredentials);
        }
        catch (ExpiredTokenException e) {
            // the exception message looks like this:
            // Token must be redeemed within 5 minutes of issuance (Service: AWSSecurityTokenService; Status Code: 400; Error Code: ExpiredTokenException; Request ID: 14af084f-91ea-11e8-910d-770fffe3b329)
            // strip off the AWS info and return it to the user
            String errMsg;
            if (e.getMessage().contains(" (")) {
                errMsg = e.getMessage().substring(0, e.getMessage().indexOf(" ("));
            } else {
                errMsg = "Token must be redeemed within a limited time of issuance";  // fallback
            }
            errors.add(ErrorUtil.buildError("application", "TKI-4010", errMsg));
            return null;
        }
        catch (AWSSecurityTokenServiceException e) {
            String errMsg;
            if (e.getMessage().contains("1 validation error detected") && e.getMessage().contains("'durationSeconds'")) {
                // the exception message can look like this:
                // 1 validation error detected: Value '43300' at 'durationSeconds' failed to satisfy constraint: Member must have value less than or equal to 43200 (Service: AWSSecurityTokenService; Status Code: 400; Error Code: ValidationError; Request ID: 5463e706-91ed-11e8-9584-734a28adb5a3)
                // extract the limit and return a nice message to the user

                if (e.getMessage().contains("Member must have value")) {
                    int beginIndex = e.getMessage().indexOf("must have value");
                    int endIndex = e.getMessage().indexOf(" (");
                    errMsg = "DurationSeconds " + e.getMessage().substring(beginIndex, endIndex);
                } else {
                    errMsg = "DurationSeconds has exceeded maximum allowable value";  // fallback
                }
                errors.add(ErrorUtil.buildError("application", "TKI-4011", errMsg));
                return null;
            } else if (e.getMessage().contains("DurationSeconds exceeds the MaxSessionDuration set for this role")) {
                // the exception message can also look like this:
                // The requested DurationSeconds exceeds the MaxSessionDuration set for this role. (Service: AWSSecurityTokenService; Status Code: 400; Error Code: ValidationError; Request ID: 67e1ff74-91f1-11e8-96f0-bd6d670eabb4)
                // strip off the AWS info and return it to the user

                if (e.getMessage().contains(" (")) {
                    errMsg = e.getMessage().substring(0, e.getMessage().indexOf(" ("));
                } else {
                    errMsg = "The requested DurationSeconds exceeds the MaxSessionDuration set for this role.";  // fallback
                }
                errors.add(ErrorUtil.buildError("application", "TKI-4012", errMsg));
                return null;
            }

            // other error are less expected so throw an exception
            throw new ProviderException(e.getMessage());
        }
        catch (EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }

        logger.info(LOGTAG + "Generate complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
        return roleAssumption;
    }

    private void duoAuth(RoleAssumption roleAssumption, String username, Credential credential, List<Error> errors)
            throws EnterpriseFieldException {

        if (duoProperties == null) {
            logger.debug(LOGTAG + "Duo disabled - not performing auth");
            return;
        }

        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "duoAuth begin");

        DuoSecurityUtil duoSecurityUtil = new DuoSecurityUtil(duoProperties);

        String factor;
        String device = null;
        String passcode = null;

        /*
         * Credential Type (Duo factor) is mandatory and can be:
         *
         * auto:
         *   Duo says: Use the out-of-band factor (push or phone) recommended by Duo as the best for the user's devices.
         *   the Credential Id will be the device ID
         *   if Id is omitted, Duo will choose "push" or "phone" as appropriate
         *
         * push:
         *   the Credential Id will be the device ID
         *   if Id is omitted, the first of the user's devices with the "push" capability will be used
         *
         * phone
         *   the Credential Id will be the device ID
         *   if Id is omitted, the first of the user's devices with the "phone" capability will be used
         *
         * passcode:
         *   the Credential Secret must be provided and will be taken as the passcode for the user
         *
         * sms
         *   not supported here - see AuthenticationSecurityAssertionProvider
         */
        String credentialType = credential.getType();
        switch (credentialType) {
            case "auto":
                factor = credentialType;
                device = credential.getId();
                if (device == null) {
                    // the Duo documentation doesn't say so, but if factor is "auto" then device must be set
                    device = "auto";
                }
                break;
            case "push":
                factor = credentialType;
                device = credential.getId();
                if (device == null) {
                    // "auto" to use the first of the user's devices with the "push" capability
                    device = "auto";
                }
                break;
            case "passcode":
                factor = credentialType;
                passcode = credential.getSecret();
                if (passcode == null) {
                    errors.add(ErrorUtil.buildError("application", "TKI-4000", "Passcode required for Duo Passcode Authentication"));
                    return;
                }
                break;
            case "phone":
                factor = credentialType;
                device = credential.getId();
                if (device == null) {
                    // "auto" to use the first of the user's devices with the "phone" capability
                    device = "auto";
                }
                break;
            default:
                errors.add(ErrorUtil.buildError("application", "TKI-4001", "Unsupported Duo Authentication method: " + credentialType));
                return;
        }

        duoSecurityUtil.authorizeUser(roleAssumption, username, factor, device, passcode, errors);

        logger.info(LOGTAG + "duoAuth complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
    }

    private SecurityAssertion retrieveSecurityAssertion(String principal)
            throws ProviderException {

        PointToPointProducer tkiServiceProducer = null;

        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "retrieveSecurityAssertion begin");
        try {
            tkiServiceProducer = (PointToPointProducer) tkiServiceProducerPool.getExclusiveProducer();
            tkiServiceProducer.setRequestTimeoutInterval(5 * 60 * 1000);  // 5 minutes

            SecurityAssertion securityAssertionQuery = (SecurityAssertion) appConfig.getObject("SecurityAssertion");
            securityAssertionQuery.setCommandName("RdbmsRequestCommand");

            SecurityAssertionQuerySpecification saqs
                    = (SecurityAssertionQuerySpecification) appConfig.getObject("SecurityAssertionQuerySpecification");
            saqs.setPrincipal(principal);
            saqs.setType("netid/password");

            long queryStart = System.currentTimeMillis();
            logger.info(LOGTAG + "security assertion query begin");
            @SuppressWarnings("unchecked")
            List<SecurityAssertion> queryResults = securityAssertionQuery.query(saqs, tkiServiceProducer);
            logger.info(LOGTAG + "security assertion query complete. Execution time (ms): " + (System.currentTimeMillis() - queryStart));

            /*
             * we expect to get a single row but handle all cases - 0, 1, >1.
             *
             * if no rows are returned then something is very wrong.
             * possibly, the client didn't make the SecurityAssertion request
             * or maybe it failed but the client continued anyway.
             * either way, we can't continue so throw an exception.
             *
             * if multiple rows are returned then the user could be running the client
             * multiple times at once.  or maybe we failed to remove old rows in a
             * previous call.  either way, we take the newest row as it is the most
             * likely to still be valid and to have not expired.
             */
            SecurityAssertion securityAssertion;
            if (queryResults.size() == 0) {
                throw new ProviderException("Missing SAML information for " + principal);
            } else if (queryResults.size() == 1) {
                securityAssertion = queryResults.get(0);
            } else {
                queryResults.sort(Comparator.comparing(sa -> sa.getCreateDatetime().toCalendar()));
                securityAssertion = queryResults.get(queryResults.size() - 1);
            }

            return securityAssertion;
        }
        catch (JMSException |
                 EnterpriseConfigurationObjectException | EnterpriseFieldException |
                 EnterpriseObjectQueryException e) {
            throw new ProviderException(e.getMessage());
        }
        finally {
            if (tkiServiceProducer != null)
                tkiServiceProducerPool.releaseProducer(tkiServiceProducer);

            logger.info(LOGTAG + "retrieveSecurityAssertion complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
        }
    }

    private void deleteSecurityAssertion(SecurityAssertion securityAssertion) throws ProviderException {
        PointToPointProducer tkiServiceProducer = null;

        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "deleteSecurityAssertion begin");
        try {
            tkiServiceProducer = (PointToPointProducer) tkiServiceProducerPool.getExclusiveProducer();
            tkiServiceProducer.setRequestTimeoutInterval(5 * 60 * 1000);  // 5 minutes
            securityAssertion.delete("Delete", tkiServiceProducer);
        }
        catch (JMSException | EnterpriseObjectDeleteException e) {
            throw new ProviderException(e.getMessage());
        }
        finally {
            if (tkiServiceProducer != null)
                tkiServiceProducerPool.releaseProducer(tkiServiceProducer);

            logger.info(LOGTAG + "deleteSecurityAssertion complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
        }
    }
}


