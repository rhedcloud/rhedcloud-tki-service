package edu.emory.it.services.tki;

import com.openii.openeai.commands.MessageMetaData;
import com.openii.openeai.commands.OpeniiRequestCommand;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.OpenEaiObject;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.jms.producer.PubSubProducer;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Error;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public abstract class TKIServiceRequestCommand extends OpeniiRequestCommand implements RequestCommand {
    static final Logger logger = LogManager.getLogger("edu.emory.it.services.tki");

    private Document genericResponseReplyDoc;
    private ProducerPool producerPool;
    protected boolean verbose;

    TKIServiceRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
 
        logger.info(getLogtag() + " Initializing " + ReleaseTag.getReleaseInfo());

        try {
            setProperties(getAppConfig().getProperties("GeneralProperties"));
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = getLogtag() + " Error retrieving 'GeneralProperties' from AppConfig: The exception is: " + e.getMessage();
            logger.fatal(errMsg, e);
            throw new InstantiationException(errMsg);
        }

        verbose = Boolean.parseBoolean(getProperties().getProperty("verbose"));

        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            genericResponseReplyDoc = xmlReader.initializeDocument(
                    getProperties().getProperty("GenericResponseDocumentUri"), getOutboundXmlValidation());
        } catch (XmlDocumentReaderException e) {
            String errMsg = getLogtag() + " Error initializing 'GenericResponseDocumentUri'.  Can't continue.";
            logger.fatal(errMsg, e);
            throw new InstantiationException(e.getMessage());
        }
        if (genericResponseReplyDoc == null) {
            String errMsg = getLogtag() + " Missing 'GenericResponseDocumentUri' property in configuration document.  Can't continue.";
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        }

        // retrieve Sync Publisher from AppConfig. This object will be used to publish
        // sync messages when generate message actions are performed in this authoritative system.
        try {
            producerPool = (ProducerPool) getAppConfig().getObject("TkiServiceSyncProducer");
        } catch (Exception e) {
            producerPool = null;
            logger.warn(getLogtag() + " No 'TkiServiceSyncProducer' PubSubProducer found in AppConfig.  "
                    + "Processing will continue but Sync Messages will not be published "
                    + "when changes are made via this Command.");
        }

        logger.info(getLogtag() + " instantiated successfully.");
    }


    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        // Get the execution start time.
        long startTime = System.currentTimeMillis();

        logger.info(getLogtag() + " [execute] - beginning message number " + messageNumber + " ...");
        String errDescription;

        MessageMetaData mmd;
        try {
            mmd = initializeMetaData(messageNumber, aMessage);
        } catch (Exception e) {
            errDescription = getLogtag() + " Exception occurred processing input message in ConsumerCommand.  Exception: " + e.getMessage();
            logExecutionComplete("Unknown", e.getMessage(), startTime);
            throw new CommandException(errDescription, e);
        }

        logger.info(getLogtag() + " [execute] - Processing a: " + mmd.getMessageCategory() + "." + mmd.getMessageObject()
                + "." + mmd.getMessageAction() + "-" + mmd.getMessageType());
        if (verbose) {
            String msgBody = getMessageBody(mmd.getInDoc());
            int idxStart = msgBody.indexOf("<Secret>");
            if (idxStart > -1) {
                int idxEnd = msgBody.indexOf("</Secret>");
                msgBody = msgBody.substring(0, idxStart + 8) + "REDACTED" + msgBody.substring(idxEnd);
            }
            logger.info(getLogtag() + " Message sent in is:\n" + msgBody);
        }

        // retrieve text portion of message passed in
        TextMessage msg = (TextMessage) aMessage;
        try {
            msg.clearBody(); // So we don't have to do it later...
        } catch (JMSException e) {
            logExecutionComplete(mmd.getMessageAction(), e.getMessage(), startTime);
            throw new CommandException(e.getMessage(), e);
        }

        // Verify that the message object we are dealing with is what we expect. If not, reply with an error.
        if (!mmd.getMessageObject().equalsIgnoreCase(getRequiredMessageObject())) {
            errDescription = getLogtag() + " Unsupported message object: " + mmd.getMessageObject() + ". "
                    + "Require '" + getRequiredMessageObject() + "'.";
            String replyContents = createErrorResponseReply(mmd, "application", "OpenEAI-1001", errDescription, null);
            logExecutionComplete(mmd.getMessageAction(), replyContents, startTime);
            return getMessage(msg, replyContents);
        }

        // check the incoming action and take appropriate steps
        String replyContents;
        switch (mmd.getMessageAction().toLowerCase()) {
            case "generate":
                replyContents = handleGenerate(mmd);
                break;
            default:
                replyContents = handleUnsupportedAction(mmd);
                break;
        }
        logExecutionComplete(mmd.getMessageAction(), replyContents, startTime);
        return getMessage(msg, replyContents);
    }

    private void logExecutionComplete(String action, String replyContents, long startTime) {
        long executionTime = System.currentTimeMillis() - startTime;
        String message = getLogtag() + " " + action + "-Request execution complete in " + executionTime + " ms"
                + (verbose ? (" with replyContents:\n" + replyContents) : "");
        logger.info(message);
    }

    abstract String getLogtag();
    abstract String getRequiredMessageObject();
    abstract String handleGenerate(MessageMetaData mmd) throws CommandException;

    private String handleUnsupportedAction(MessageMetaData mmd) {
        String errDescription = "Unsupported message action: " + mmd.getMessageAction() + ". "
                + getLogtag() + " only supports actions: "
                + String.join(", ", "Generate") + ".";
        return createErrorResponseReply(mmd, "application", "OpenEAI-1002", errDescription, null);
    }

    /**
     * Based on the 'messageAction' associated to the request we determine which
     * 'PrimedXmlDocument' associated to the message object being operated on
     * should be returned. This is specified in the Command's deployment
     * descriptor on each message object.
     * <p>
     * For example, our command supports
     * com.any-erp-vendor.Person/BasicPerson/1.0/Query-Request
     * and
     * com.any-erp-vendor.Person/BasicPerson/1.0/Create-Request.
     * We know based on the OpenEAI Message Protocol that the response to a
     * com.any-erp-vendor.Person/BasicPerson/1.0/Query-Request
     * is
     * com.any-erp-vendor.Person/BasicPerson/1.0/Provide-Reply.
     * We also know that the response to a
     * com.any-erp-vendor.Person/BasicPerson/1.0/Create-Request
     * is a
     * org.openeai.CoreMessaging/1.0/Response-Reply
     * with the appropriate status, action and error information filled in.
     * <p>
     * Because of all this knowledge, we configure the message objects used by
     * this Command to have the appropriate 'PrimedXmlDocument' entries that
     * allows us to simply get the appropriate reply document from the object
     * based on the action. If the Action is 'Query' we'll be returning the
     * 'primed' Provide document associated to the object. If the action is
     * anything else, we'll be returning the 'primed' generic Response document
     * associated to the object.
     **/
    private Document getReplyDoc(String msgAction, String msgObject, String msgRelease) throws CommandException {

        try {
            XmlEnterpriseObject xeo = (XmlEnterpriseObject) getAppConfig().getObject(msgObject + "." + generateRelease(msgRelease));
            Document rDoc = xeo.getResponseDoc();

            if (rDoc == null) {
                String message = getLogtag() + " Could not find a reply document for the '" + msgObject + "-" + msgAction + "' request.";
                logger.fatal(message);
                throw new CommandException(message);
            } else if (verbose) {
                logger.info(getLogtag() + " Found a reply document for the '" + msgObject + "-" + msgAction + "' request.");
            }
            return (Document) rDoc.clone();
        } catch (CommandException e) {
            throw e;
        } catch (Exception e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    String createRegularResponseReply(ActionableEnterpriseObject eo, MessageMetaData mmd, List<Error> errors)
            throws CommandException, EnterpriseLayoutException {

        Document replyDoc = getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease());

        if (!errors.isEmpty())
            return buildReplyDocumentWithErrors(mmd.getControlArea(), replyDoc, errors);

        if (eo == null) {
            if (verbose)
                logger.debug(getLogtag() + " NULL generate response ... returning empty reply");

            // remove the contents of the DataArea element from the primed Provide-Reply document
            // to give an empty reply
            replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());
        } else {
            publishSync(mmd.getMessageObject(), mmd.getMessageAction(), eo);

            if (verbose)
                logger.debug(getLogtag() + " Adding " + mmd.getMessageObject() + " object to the Response-Reply document.");

            // remove the contents of the DataArea element from the primed Response-Reply document
            // before adding our output Element
            replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());

            eo.setOutputLayoutManager(eo.getOutputLayoutManager("xml"));
            Element dataAreaContent = (Element) eo.buildOutputFromObject();
            replyDoc.getRootElement().getChild(DATA_AREA).addContent(dataAreaContent);
        }
        return buildReplyDocument(mmd.getControlArea(), replyDoc);
    }

    String createErrorResponseReply(MessageMetaData mmd, String errType, String errNumber, String errDescription, Exception e) {
        Document genericDoc = (Document) genericResponseReplyDoc.clone();

        List<Error> errors = new ArrayList<>();
        errors.add(buildError(errType, errNumber, errDescription));

        logger.fatal(getLogtag() + " " + errDescription, e);
        if (verbose) {
            logger.fatal(getLogtag() + " Message sent in is:\n" + getMessageBody(mmd.getInDoc()));
        }

        if (e != null) {
            // copied from RequestCommandImpl.buildReplyDocumentWithErrors(Element senderControlArea, Document replyDoc, List errors, Throwable e)
            // to deal with the unicode characters that can be in the LDAP API exception messages
            ByteArrayOutputStream bw = new ByteArrayOutputStream();
            PrintWriter pw = new PrintWriter(bw, true);
            e.printStackTrace(pw);

            // strip unicode from m1 and m2
            String m1 = "";
            if (e.getMessage() != null) {
                m1 = e.getMessage().replaceAll("[^\\x01-\\x7F]", "");
            }
            String m2 = bw.toString().replaceAll("[^\\x01-\\x7F]", "");

            errors.add(buildError("system", "COMMAND-1001", "Exception: " + m1 + "\n" + m2));
        }

        return this.buildReplyDocumentWithErrors(mmd.getControlArea(), genericDoc, errors);
    }

    private void publishSync(String msgObject, String action, ActionableEnterpriseObject jeo)
            throws CommandException {

        if (producerPool == null) {
            logger.info(getLogtag() + " No configured Sync Publisher so syncs CANNOT be published."
                    + " Check the application's config doc if you want to publish sync messages.");
            return;
        }

        PubSubProducer pubSub = null;
        String modality = "Sync";

        try {
            pubSub = (PubSubProducer) producerPool.getExclusiveProducer();

            logger.info(getLogtag() + " Publishing '" + msgObject + "-" + action + "-" + modality + "' message");

            if (action.equalsIgnoreCase("generate")) {
                if (verbose)
                    logger.info(getLogtag() + " Data:\n" + jeo.toXmlString());
                jeo.createSync(pubSub);
            } else {
                throw new CommandException(getLogtag() + " Can not publish Sync for action: " + action);
            }

            logger.info(getLogtag() + " Published '" + msgObject + "-" + action + "-" + modality + "' message");
        } catch (CommandException e) {
            throw e;
        } catch (Exception e) {
            throw new CommandException(e);
        } finally {
            try {
                if (pubSub != null)
                    producerPool.releaseProducer(pubSub);
            } catch (Exception e) {
                logger.error(getLogtag() + " Error releasing PubSubProducer");
            }
        }
    }
}
