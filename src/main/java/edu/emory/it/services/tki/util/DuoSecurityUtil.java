package edu.emory.it.services.tki.util;

import com.duosecurity.client.Http;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Response;
import edu.emory.moa.jmsobjects.authentication.v1_0.RoleAssumption;
import edu.emory.moa.jmsobjects.authentication.v1_0.SecurityAssertion;
import edu.emory.moa.objects.resources.v1_0.Device;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.objects.resources.Error;

import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;

/**
 * Duo interactions - preauth and auth requests.
 */
public class DuoSecurityUtil {
    private static final Logger logger = LogManager.getLogger("edu.emory.it.services.tki");
    private static final String LOGTAG = "[DuoSecurityUtil] ";

    private static final ObjectMapper MAPPER = new ObjectMapper().findAndRegisterModules();

    private static final String DUO_API_HOST_PROPERTY = "DUO_API_HOST";
    private static final String DUO_INTEGRATION_KEY_PROPERTY = "DUO_INTEGRATION_KEY";
    private static final String DUO_SECRET_KEY_PROPERTY = "DUO_SECRET_KEY";

    private final Properties properties;

    public DuoSecurityUtil(Properties p) {
        properties = p;
    }

    public static Properties getDuoProperties(Properties generalProperties) throws EnterpriseConfigurationObjectException {
        // make sure GeneralProperties from the deployment descriptor contains all needed properties
        // if the API host is not set then Duo has been disabled
        if (generalProperties.getProperty(DUO_API_HOST_PROPERTY) == null) {
            logger.info(LOGTAG + "Duo disabled - " + DUO_API_HOST_PROPERTY + " not found");
            return null;
        }
        if (generalProperties.getProperty(DUO_INTEGRATION_KEY_PROPERTY) == null)
            throw new EnterpriseConfigurationObjectException("GeneralProperties is missing the required property: " + DUO_INTEGRATION_KEY_PROPERTY);
        if (generalProperties.getProperty(DUO_SECRET_KEY_PROPERTY) == null)
            throw new EnterpriseConfigurationObjectException("GeneralProperties is missing the required property: " + DUO_SECRET_KEY_PROPERTY);

        Properties duoProperties = new Properties();

        duoProperties.setProperty(DUO_API_HOST_PROPERTY, generalProperties.getProperty(DUO_API_HOST_PROPERTY));
        duoProperties.setProperty(DUO_INTEGRATION_KEY_PROPERTY, generalProperties.getProperty(DUO_INTEGRATION_KEY_PROPERTY));
        duoProperties.setProperty(DUO_SECRET_KEY_PROPERTY, generalProperties.getProperty(DUO_SECRET_KEY_PROPERTY));

        return duoProperties;
    }

    // https://duo.com/docs/authapi#/preauth
    public void getAvailableAuthenticationFactors(SecurityAssertion securityAssertion, List<Error> errors) throws EnterpriseFieldException {
        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "getAvailableAuthenticationFactors begin for security assertion principal " + securityAssertion.getPrincipal());

        String duoApiHost = properties.getProperty(DUO_API_HOST_PROPERTY);
        String duoIntegrationKey = properties.getProperty(DUO_INTEGRATION_KEY_PROPERTY);
        String duoSecretKey = properties.getProperty(DUO_SECRET_KEY_PROPERTY);

        JsonNode authenticationFactors;
        try {
            Http preauthRequest = new Http("POST", duoApiHost, "/auth/v2/preauth");
            preauthRequest.addParam("username", securityAssertion.getPrincipal());
            preauthRequest.signRequest(duoIntegrationKey, duoSecretKey);

            long postStart = System.currentTimeMillis();
            logger.info(LOGTAG + "Duo (preauth) POST to " + duoApiHost);
            Response response = preauthRequest.executeHttpRequest();
            logger.info(LOGTAG + "Duo (preauth) POST complete.  Execution time (ms): " + (System.currentTimeMillis() - postStart));

            if (!response.isSuccessful()) {
                // strange and unexpected error - return a message that will likely result in a support call
                // the log will contain the same message the user gets plus the body of the response
                String errDescription = "Duo preauth not successful: username=" + securityAssertion.getPrincipal()
                        + " code=" + response.code() + " message=" + response.message();
                errors.add(ErrorUtil.buildError("application", "TKI-5000", errDescription));
                logger.error(LOGTAG + errDescription + "\n" + response.body().string());
                return;
            }

            JsonNode result = MAPPER.readTree(URLDecoder.decode(response.body().string(), StandardCharsets.UTF_8.name()));

            logger.info(LOGTAG + "Duo preauth response for user '" + securityAssertion.getPrincipal() + "' is " + result.toString());

            if (result.has("stat") && result.get("stat").asText().equalsIgnoreCase("OK")) {
                if (result.has("response") && result.get("response").has("result")) {
                    /*
                     * auth: The user is known and permitted to authenticate.
                     *       Your client application should use the /auth endpoint to perform authentication.
                     * allow: The user is configured to bypass secondary authentication.
                     *        Your client application should immediately grant access.
                     * deny: The user is not permitted to authenticate at this time.
                     *       Your client application should immediately deny access.
                     * enroll: The user is not known to Duo and needs to enroll.
                     *         Your application should deny access.
                     */
                    String preauthResult = result.get("response").get("result").asText();
                    if (!preauthResult.equalsIgnoreCase("auth")
                            && !preauthResult.equalsIgnoreCase("allow")) {
                        errors.add(ErrorUtil.buildError("application", "TKI-5001", "User is not permitted to authenticate"));
                        return;
                    }
                }
                if (result.has("response") && result.get("response").has("devices")) {
                    authenticationFactors = result.get("response").get("devices");
                    logger.debug(LOGTAG + "Duo preauth authenticationFactors " + authenticationFactors);
                } else {
                    logger.info(LOGTAG + "Duo preauth response for user '" + securityAssertion.getPrincipal() + "' says no devices");
                    return;  // no errors but also no devices
                }
            } else {
                // access has NOT been granted.  status_msg may contain a reason
                String statusMsg = "Duo preauth not allowed";
                if (result.has("response") && result.get("response").has("status_msg")) {
                    statusMsg += ": " + result.get("response").get("status_msg").asText();
                }
                errors.add(ErrorUtil.buildError("application", "TKI-5002", statusMsg));
                logger.error(LOGTAG + "Duo (preauth) not OK: " + response);
                return;
            }
        } catch (Exception e) {
            // not quite right but very convenient to reuse EnterpriseFieldException
            throw new EnterpriseFieldException(e);
        }

        for (JsonNode deviceNode : authenticationFactors) {
            Device device = securityAssertion.newDevice();
            securityAssertion.addDevice(device);

            logger.debug(LOGTAG + "authenticationFactors deviceNode: " + deviceNode.toString());

            if (deviceNode.has("device")) {
                logger.debug(LOGTAG + "authenticationFactors deviceNode/device: " + deviceNode.get("device").textValue());
                device.setDeviceId(deviceNode.get("device").textValue());
            }
            if (deviceNode.has("capabilities")) {
                for (JsonNode capabilitiesNode : deviceNode.get("capabilities")) {
                    logger.debug(LOGTAG + "authenticationFactors deviceNode/capabilities: " + capabilitiesNode.textValue());
                    device.addCapability(capabilitiesNode.textValue());
                }
            }
            if (deviceNode.has("display_name")) {
                logger.debug(LOGTAG + "authenticationFactors deviceNode/display_name: " + deviceNode.get("display_name").textValue());
                device.setDisplayName(deviceNode.get("display_name").textValue());
            }
            if (deviceNode.has("number")) {
                logger.debug(LOGTAG + "authenticationFactors deviceNode/number: " + deviceNode.get("display_name").textValue());
                device.setNumber(deviceNode.get("number").textValue());
            }
            if (deviceNode.has("type")) {
                logger.debug(LOGTAG + "authenticationFactors deviceNode/type: " + deviceNode.get("type").textValue());
                device.setType(deviceNode.get("type").textValue());
            }
        }

        logger.info(LOGTAG + "getAvailableAuthenticationFactors complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
    }

    // https://duo.com/docs/authapi#/auth
    public void authorizeUser(RoleAssumption roleAssumption,
                              String username, String factor, String device, String passcode,
                              List<Error> errors)
            throws EnterpriseFieldException {

        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "authorizeUser begin for username " + username);

        String duoApiHost = properties.getProperty(DUO_API_HOST_PROPERTY);
        String duoIntegrationKey = properties.getProperty(DUO_INTEGRATION_KEY_PROPERTY);
        String duoSecretKey = properties.getProperty(DUO_SECRET_KEY_PROPERTY);

        boolean authorized = false;
        boolean isTimeout = false;
        String statusMsg = null;
        try {
            Http authRequest = new Http("POST", duoApiHost, "/auth/v2/auth", 30); //30 second timeout instead of default of 60
            authRequest.addParam("username", username);
            authRequest.addParam("factor", factor);
            if (device != null) {
                authRequest.addParam("device", device);
            }
            if (passcode != null) {
                authRequest.addParam("passcode", passcode);
            }
            authRequest.signRequest(duoIntegrationKey, duoSecretKey);

            long postStart = System.currentTimeMillis();
            logger.info(LOGTAG + "Duo (auth) POST to " + duoApiHost);
            Response response = authRequest.executeHttpRequest();
            logger.info(LOGTAG + "Duo (auth) POST complete.  Execution time (ms): " + (System.currentTimeMillis() - postStart) );

            if (response.isSuccessful()) {
                // use the same Charset for decoding that okhttp uses when stringifying the response body
                Charset charset = response.body().contentType() != null
                        ? response.body().contentType().charset(StandardCharsets.UTF_8) : StandardCharsets.UTF_8;
                JsonNode result = MAPPER.readTree(URLDecoder.decode(response.body().string(), charset.name()));

                logger.debug(LOGTAG + "Duo auth response for user '" + username + "' is " + result.toString());

                if (result.has("stat") && result.get("stat").asText().equalsIgnoreCase("OK")
                        && result.has("response")
                        && result.get("response").has("result")
                        && result.get("response").get("result").asText().equalsIgnoreCase("allow")) {

                    // Duo has granted access to the user
                    authorized = true;

                    if (result.get("response").has("trusted_device_token")) {
                        roleAssumption.setToken(result.get("response").get("trusted_device_token").asText());
                    }
                } else if (result.has("response") && result.get("response").has("status_msg")) {
                    // access has NOT been granted.  status_msg may contain a reason
                    statusMsg = result.get("response").get("status_msg").asText();
                }
            } else {
                logger.error(LOGTAG + "Duo (auth) POST not successful: " + response + "\n" + response.body().string());
            }
        } catch (SocketTimeoutException e) {
            isTimeout = true;
        } catch (Exception e) {
            // not quite right but very convenient to reuse EnterpriseFieldException
            throw new EnterpriseFieldException(e);
        }

        if (!authorized) {
            String statusCode, msg;
            if (isTimeout) {
                statusCode = "TKI-5005";
                msg = "Time out retrieving duo authentication.  Please make sure authentication device is turned on.";

            } else if (statusMsg != null && statusMsg.contains("Incorrect passcode")) {
                statusCode = "TKI-5006";
                msg = statusMsg;
            } else {
                statusCode = "TKI-5003";
                msg = "Duo authorization denied for user " + username;
                if (statusMsg != null)
                    msg += ": " + statusMsg;
                logger.error(LOGTAG + msg);
            }

            errors.add(ErrorUtil.buildError("application", statusCode, msg));
        }

        logger.info(LOGTAG + "authorizeUser complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
    }

    public void sms(String username, String device, List<Error> errors) throws EnterpriseFieldException {
        String duoApiHost = properties.getProperty(DUO_API_HOST_PROPERTY);
        String duoIntegrationKey = properties.getProperty(DUO_INTEGRATION_KEY_PROPERTY);
        String duoSecretKey = properties.getProperty(DUO_SECRET_KEY_PROPERTY);

        boolean passcodesSent = false;
        String statusMsg = null;

        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "sms begin");

        try {
            Http authRequest = new Http("POST", duoApiHost, "/auth/v2/auth");
            authRequest.addParam("username", username);
            authRequest.addParam("factor", "sms");
            authRequest.addParam("device", device);
            authRequest.signRequest(duoIntegrationKey, duoSecretKey);

            long postStart = System.currentTimeMillis();
            logger.info(LOGTAG + "Duo (auth) POST to " + duoApiHost);
            Response response = authRequest.executeHttpRequest();
            logger.info(LOGTAG + "Duo (auth) POST complete.  Execution time (ms): " + (System.currentTimeMillis()-postStart));

            // use the same Charset for decoding that okhttp uses when stringifying the response body
            Charset charset = response.body().contentType() != null
                    ? response.body().contentType().charset(StandardCharsets.UTF_8) : StandardCharsets.UTF_8;
            JsonNode result = MAPPER.readTree(URLDecoder.decode(response.body().string(), charset.name()));

            if (response.isSuccessful()) {
                logger.debug(LOGTAG + "Duo auth response for user '" + username + "' is " + result.toString());

                // {"stat":"OK", "response":{"result":"deny","status":"sent","status_msg":"New SMS passcodes sent."}}

                if (result.has("stat") && result.get("stat").asText().equalsIgnoreCase("OK")
                        && result.has("response")
                        && result.get("response").has("status")
                        && result.get("response").get("status").asText().equalsIgnoreCase("sent")) {

                    // Duo has sent passcodes to the user
                    passcodesSent = true;
                } else if (result.has("response") && result.get("response").has("status_msg")) {
                    // passcodes were NOT sent.  status_msg may contain a reason
                    statusMsg = result.get("response").get("status_msg").asText();
                }
            } else {
                logger.error(LOGTAG + "Duo (sms auth) POST not successful: " + response + "\n" + result.toString());

                // messages can be like this, in which case we can send info back to the user
                // {"code": 40002, "message": "Invalid request parameters", "message_detail": "device", "stat": "FAIL"}

                if (result.has("message") && result.has("message_detail")) {
                    statusMsg = result.get("message").asText() + ": " + result.get("message_detail").asText();
                }
            }
        } catch (Exception e) {
            // not quite right but very convenient to reuse EnterpriseFieldException
            throw new EnterpriseFieldException(e);
        }

        if (!passcodesSent) {
            String msg = "Duo passcodes denied for user " + username;
            if (statusMsg != null)
                msg += ": " + statusMsg;
            logger.error(LOGTAG + msg);
            errors.add(ErrorUtil.buildError("application", "TKI-5004", msg));
        }

        logger.info(LOGTAG + "sms complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
    }
}
