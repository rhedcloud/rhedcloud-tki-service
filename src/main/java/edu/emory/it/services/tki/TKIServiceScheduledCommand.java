package edu.emory.it.services.tki;

import com.openii.openeai.commands.OpeniiScheduledCommand;
import edu.emory.moa.jmsobjects.authentication.v1_0.SecurityAssertion;
import edu.emory.moa.objects.resources.v1_0.SecurityAssertionQuerySpecification;
import org.apache.logging.log4j.Logger;
import org.openeai.afa.ScheduledCommand;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectQueryException;

import javax.jms.JMSException;
import java.util.List;
import java.util.stream.Collectors;

public class TKIServiceScheduledCommand extends OpeniiScheduledCommand implements ScheduledCommand {
    private static final String LOGTAG = "[TKIServiceScheduledCommand] ";
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger("edu.emory.it.services.tki");

    private ProducerPool m_tkiProducerPool;

    public TKIServiceScheduledCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + "Initializing ...");

        // Get the producer pools we specified in the deployment descriptor from AppConfig
        try {
            m_tkiProducerPool = (ProducerPool) getAppConfig().getObject("TkiServiceRequestProducer");
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving the TkiServiceRequestProducer producer pool from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        logger.info(LOGTAG + "Initialization complete.");
    }

    @Override
    public int execute() throws ScheduledCommandException {
        long start = System.currentTimeMillis();
        long requestStart;
        long requestElapsedTime;

        logger.info(LOGTAG + "Job started to delete expired assertions.");

        PointToPointProducer tkiProducer = null;
        try {
            tkiProducer = (PointToPointProducer) m_tkiProducerPool.getExclusiveProducer();

            SecurityAssertion securityAssertion = (SecurityAssertion) getAppConfig().getObject("SecurityAssertion.v1_0");
            securityAssertion.setCommandName("RdbmsRequestCommand");

            SecurityAssertionQuerySpecification securityAssertionQuerySpecification
                    = (SecurityAssertionQuerySpecification) getAppConfig().getObject("SecurityAssertionQuerySpecification.v1_0");

            logger.info(LOGTAG + "Retrieving assertions ...");
            requestStart = System.currentTimeMillis();
            @SuppressWarnings("unchecked")
            List<SecurityAssertion> assertions = securityAssertion.query(securityAssertionQuerySpecification, tkiProducer);
            requestElapsedTime = System.currentTimeMillis() - requestStart;
            logger.info(LOGTAG + "Retrieved assertions in " + requestElapsedTime + " ms.");

            long fiveMinutesAgo = System.currentTimeMillis() - (5 * 60 * 1000);

            List<SecurityAssertion> assertionsToDelete = assertions.stream()
                    .filter(assertion -> assertion.getCreateDatetime().toCalendar().getTimeInMillis() < fiveMinutesAgo)
                    .collect(Collectors.toList());

            logger.info(LOGTAG + "Found " + assertionsToDelete.size() + " assertions to delete.");

            // delete needs a bit more time to complete
            tkiProducer.setRequestTimeoutInterval(10 * 60 * 1000); // 10 minutes

            for (SecurityAssertion assertion : assertionsToDelete) {
                logger.info(LOGTAG + "Sending a Delete-Request for assertion id " + assertion.getSecurityAssertionId());
                requestStart = System.currentTimeMillis();
                assertion.delete("Delete", tkiProducer);
                requestElapsedTime = System.currentTimeMillis() - requestStart;
                logger.info(LOGTAG + "Deleted assertion id " + assertion.getSecurityAssertionId() + " in " + requestElapsedTime + " ms.");
            }
        }
        catch (JMSException e) {
            String errMsg = "An error occurred while getting the TKI Service Producer. "
                    + "The exception is " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new ScheduledCommandException(errMsg);
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred while getting SecurityAssertion or SecurityAssertionQuerySpecification objects. "
                    + "The exception is " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new ScheduledCommandException(errMsg);
        }
        catch (EnterpriseObjectQueryException e) {
            String errMsg = "An error occurred while sending Query-Request to TKI Service Producer. "
                    + "The exception is " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new ScheduledCommandException(errMsg);
        }
        catch (EnterpriseObjectDeleteException e) {
            String errMsg = "An error occurred while sending Delete-Request to TKI Service Producer. "
                    + "The exception is " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new ScheduledCommandException(errMsg);
        }
        finally {
            if (tkiProducer != null)
                m_tkiProducerPool.releaseProducer(tkiProducer);
        }

        long timeElapsed = System.currentTimeMillis() - start;
        logger.info(LOGTAG + "Execution done ... Time Elapse (ms): " + timeElapsed);

        return 0;
    }
}
