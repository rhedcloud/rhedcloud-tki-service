package edu.emory.it.services.tki.provider;

import edu.emory.moa.jmsobjects.authentication.v1_0.SecurityAssertion;
import edu.emory.moa.objects.resources.v1_0.AccountAlias;
import edu.emory.moa.objects.resources.v1_0.Datetime;
import edu.emory.moa.objects.resources.v1_0.Device;
import edu.emory.moa.objects.resources.v1_0.RoleDetail;
import edu.emory.moa.objects.resources.v1_0.SecurityAssertionRequisition;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.objects.resources.Error;

import java.util.List;

public class ExampleSecurityAssertionProvider implements SecurityAssertionProvider {
    private AppConfig appConfig;

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        if (aConfig == null)
            throw new ProviderException("Must have an AppConfig");
        appConfig = aConfig;
    }

    @Override
    public SecurityAssertion generate(SecurityAssertionRequisition requisition, List<Error> errors) throws ProviderException {
        SecurityAssertion securityAssertion;
        try {
            securityAssertion = (SecurityAssertion) appConfig.getObjectByType(SecurityAssertion.class.getName());
        } catch (EnterpriseConfigurationObjectException e) {
            throw new ProviderException(e.getMessage());
        }

        try {
            securityAssertion.setType(requisition.getType());
            securityAssertion.setPrincipal(requisition.getPrincipal());
            Device device = securityAssertion.newDevice();
            device.setDeviceId("DPQ0DEIXZELI9YN9Q9JX");
            device.addCapability("auto");
            device.addCapability("phone");
            device.setDisplayName("Landline (XXX-867-5309)");
            device.setNumber("615-867-5309");
            device.setType("phone");
            securityAssertion.addDevice(device);
            device = securityAssertion.newDevice();
            device.setDeviceId("DP9XX99KAJCKBMWKI9H9");
            device.addCapability("auto");
            device.addCapability("push");
            device.addCapability("sms");
            device.addCapability("phone");
            device.addCapability("mobile_otp");
            device.setDisplayName("Android (XXX-867-5309)");
            device.setNumber("865-867-5309");
            device.setType("phone");
            securityAssertion.addDevice(device);
            RoleDetail roleDetail = securityAssertion.newRoleDetail();
            AccountAlias accountAlias = roleDetail.newAccountAlias();
            accountAlias.setAccountId("123456789012");
            accountAlias.setName("aws-test-1");
            roleDetail.setAccountAlias(accountAlias);
            roleDetail.setPrincipalArn("arn:aws:iam::123456789012:saml-provider/Site_QA_IDP");
            roleDetail.setRoleArn("arn:aws:iam::123456789012:role/Auditor");
            securityAssertion.addRoleDetail(roleDetail);
            roleDetail = securityAssertion.newRoleDetail();
            accountAlias = roleDetail.newAccountAlias();
            accountAlias.setAccountId("123456789012");
            accountAlias.setName("aws-test-1");
            roleDetail.setAccountAlias(accountAlias);
            roleDetail.setPrincipalArn("arn:aws:iam::123456789012:saml-provider/Site_QA_IDP");
            roleDetail.setRoleArn("arn:aws:iam::123456789012:role/Administrator");
            securityAssertion.addRoleDetail(roleDetail);
            Datetime datetime = securityAssertion.newCreateDatetime();
            datetime.setYear("1999");  // Y2K
            datetime.setMonth("12");
            datetime.setDay("31");
            datetime.setHour("11");
            datetime.setMinute("59");
            datetime.setSecond("59");
            datetime.setSubSecond("999");
            datetime.setTimezone("America/Denver");
            securityAssertion.setCreateDatetime(datetime);
        } catch (EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }

        return securityAssertion;
    }
}
