package edu.emory.it.services.tki.provider;

import edu.emory.moa.jmsobjects.authentication.v1_0.SecurityAssertion;
import edu.emory.moa.objects.resources.v1_0.SecurityAssertionRequisition;
import org.openeai.config.AppConfig;
import org.openeai.moa.objects.resources.Error;

import java.util.List;

public interface SecurityAssertionProvider {
    /**
     * Initialize the provider.
     *
     * @param aConfig, an AppConfig object with all this provider needs.
     * @throws ProviderException on error.
     */
    void init(AppConfig aConfig) throws ProviderException;

    /**
     * Handle the generate command.
     *
     * @param requisition incoming requisition
     * @param errors possible errors
     * @return the SecurityAssertion
     * @throws ProviderException on error
     */
    SecurityAssertion generate(SecurityAssertionRequisition requisition, List<Error> errors) throws ProviderException;
}
