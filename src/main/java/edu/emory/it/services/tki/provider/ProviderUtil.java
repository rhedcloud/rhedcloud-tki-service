package edu.emory.it.services.tki.provider;

import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.producer.ProducerPool;

class ProviderUtil {
    static ProducerPool getProducerPool(String producerName, AppConfig appConfig, String logtag) throws ProviderException {
        try {
            return (ProducerPool) appConfig.getObject(producerName);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving the " + producerName + " producer pool from AppConfig."
                          + " The exception is: " + e.getMessage();
            OpenEaiObject.logger.fatal(logtag + errMsg);
            throw new ProviderException(errMsg);
        }
    }
}
