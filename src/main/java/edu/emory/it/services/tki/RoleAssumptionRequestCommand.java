package edu.emory.it.services.tki;

import com.openii.openeai.commands.MessageMetaData;
import edu.emory.it.services.tki.provider.ProviderException;
import edu.emory.it.services.tki.provider.RoleAssumptionProvider;
import edu.emory.moa.jmsobjects.authentication.v1_0.RoleAssumption;
import edu.emory.moa.objects.resources.v1_0.RoleAssumptionRequisition;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.objects.resources.Error;

import java.util.ArrayList;
import java.util.List;

/**
 * Command that handles Role Assumption requests for the Temporary Key Issuance (TKI) Service.
 */
public class RoleAssumptionRequestCommand extends TKIServiceRequestCommand implements RequestCommand {

    private static final String PROVIDER_CLASS_NAME = "roleAssumptionProviderClassName";

    private RoleAssumptionProvider provider;

    public RoleAssumptionRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        // set the initial provider, this may change when/if the configuration is changed during execute
        provider = initializeProvider();
    }

    @Override
    String handleGenerate(MessageMetaData mmd) throws CommandException {
        // retrieve a message object from AppConfig and populate it with data from the incoming message
        RoleAssumptionRequisition requisition;
        try {
            requisition = (RoleAssumptionRequisition) getAppConfig().getObjectByType(RoleAssumptionRequisition.class.getName());
            requisition.buildObjectFromInput(mmd.getData());
        } catch (EnterpriseConfigurationObjectException e) {
            String errDesc = getLogtag() + " Error retrieving a RoleAssumptionRequisition " +
                    "object from AppConfig: The exception is: " + e.getMessage();
            logger.fatal(errDesc);
            return createErrorResponseReply(mmd, "application", "TKI-1000", errDesc, e);
        } catch (EnterpriseLayoutException e) {
            String errDesc = getLogtag() + " Error building a RoleAssumptionRequisition " +
                    "object from request data: The exception is: " + e.getMessage();
            logger.fatal(errDesc);
            return createErrorResponseReply(mmd, "application", "TKI-1001", errDesc, e);
        }

        try {
            List<Error> errors = new ArrayList<>();
            RoleAssumption roleAssumption = provider.generate(requisition, errors);

            return createRegularResponseReply(roleAssumption, mmd, errors);

        } catch (ProviderException e) {
            String errDescription = "Exception occurred generating the '" + mmd.getMessageObject() + "' object.";
            return createErrorResponseReply(mmd, "application", "TKI-1002", errDescription, e);
        } catch (EnterpriseLayoutException e) {
            String errDescription = "Exception occurred creating the '" + mmd.getMessageObject()
                    + "' object(s) from the data passed back from the provider.";
            return createErrorResponseReply(mmd, "application", "TKI-1003", errDescription, e);
        }
    }


    @Override
    String getLogtag() { return "[RoleAssumptionRequestCommand]"; }
    @Override
    String getRequiredMessageObject() { return "RoleAssumption"; }

    private RoleAssumptionProvider initializeProvider() throws InstantiationException {
        String providerClassName = getProperties().getProperty(PROVIDER_CLASS_NAME);
        if (providerClassName == null || providerClassName.equals("")) {
            String errMsg = getLogtag() + " No " + PROVIDER_CLASS_NAME + " property specified. Can't continue.";
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        }

        try {
            logger.info(getLogtag() + " Getting provider class for name: " + providerClassName);
            Class providerClass = Class.forName(providerClassName);
            RoleAssumptionProvider providerInstance = (RoleAssumptionProvider) providerClass.newInstance();
            providerInstance.init(getAppConfig());
            logger.info(getLogtag() + " Initialization complete for provider " + providerClassName);

            return providerInstance;
        } catch (ClassNotFoundException e) {
            String errMsg = getLogtag() + " Class named " + providerClassName + "not found on the classpath.  The exception is: " + e.getMessage();
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        } catch (IllegalAccessException e) {
            String errMsg = getLogtag() + " An error occurred getting a class for name: " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        } catch (ProviderException e) {
            String errMsg = getLogtag() + " An error occurred initializing the provider " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        }
    }
}
