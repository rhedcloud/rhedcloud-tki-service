package edu.emory.it.services.tki.provider;

import edu.emory.moa.jmsobjects.authentication.v1_0.RoleAssumption;
import edu.emory.moa.objects.resources.v1_0.RoleAssumptionRequisition;
import org.openeai.config.AppConfig;
import org.openeai.moa.objects.resources.Error;

import java.util.List;

public interface RoleAssumptionProvider {
    /**
     * Initialize the provider.
     *
     * @param aConfig, an AppConfig object with all this provider needs.
     * @throws ProviderException on error.
     */
    void init(AppConfig aConfig) throws ProviderException;

    /**
     * Handle the generate command.
     *
     * @param requisition incoming requisition
     * @param errors possible errors
     * @return the RoleAssumption
     * @throws ProviderException on error
     */
    RoleAssumption generate(RoleAssumptionRequisition requisition, List<Error> errors) throws ProviderException;
}
