package edu.emory.it.services.tki.provider;

import edu.emory.moa.jmsobjects.authentication.v1_0.RoleAssumption;
import edu.emory.moa.objects.resources.v1_0.AssumedRoleUser;
import edu.emory.moa.objects.resources.v1_0.Credentials;
import edu.emory.moa.objects.resources.v1_0.RoleAssumptionRequisition;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.objects.resources.Error;

import java.util.List;

public class ExampleRoleAssumptionProvider implements RoleAssumptionProvider {
    private AppConfig appConfig;

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        if (aConfig == null)
            throw new ProviderException("Must have an AppConfig");
        appConfig = aConfig;
    }

    @Override
    public RoleAssumption generate(RoleAssumptionRequisition requisition, List<Error> errors) throws ProviderException {
        RoleAssumption roleAssumption;
        try {
            roleAssumption = (RoleAssumption) appConfig.getObjectByType(RoleAssumption.class.getName());
        } catch (EnterpriseConfigurationObjectException e) {
            throw new ProviderException(e.getMessage());
        }

        try {
            roleAssumption.setAudience("https://signin.aws.amazon.com/saml");
            roleAssumption.setIssuer("https://login.rhedcloud.org:4443/idp/shibboleth");
            roleAssumption.setNameQualifier("9z8y/a1b2c3d4e5f6g7h8i9j0=");
            roleAssumption.setSubject("a1b2c3d4e5f6g7h8i9j0");
            roleAssumption.setSubjectType("transient");
            AssumedRoleUser assumedRoleUser = roleAssumption.newAssumedRoleUser();
            assumedRoleUser.setArn("arn:aws:sts::123456789012:assumed-role/Administrator/P9999999");
            assumedRoleUser.setAssumedRoleId("AROAIX3IMMANUCZIKI999:P9999999");
            roleAssumption.setAssumedRoleUser(assumedRoleUser);
            Credentials credentials = roleAssumption.newCredentials();
            credentials.setAccessKeyId("ASIAJ263LHWU4BFKT599");
            credentials.setExpiration("Wed Apr 11 20:09:36 CDT 2018");
            credentials.setSecretAccessKey("a1b2c3d4e5f6g7h8i9j0/9z8y");
            credentials.setSessionToken("F1o2Y3==");
            roleAssumption.setCredentials(credentials);
            roleAssumption.setToken("a1b2c3d4e5f6g7h8i9j0/7x6w");
        } catch (EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }

        return roleAssumption;
    }
}
