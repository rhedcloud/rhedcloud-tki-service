package edu.emory.it.services.tki.util;

import org.openeai.moa.objects.resources.Error;

public class ErrorUtil {
    // extracted from ConsumerCommand
    public static Error buildError(String errType, String errNumber, String errDescription) {
        Error anError = new Error();
        anError.setType(errType);
        anError.setErrorNumber(errNumber);
        anError.setErrorDescription(errDescription);
        return anError;
    }
}
