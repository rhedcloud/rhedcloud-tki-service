package edu.emory.it.services.tki.provider;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Account;
import com.amazon.aws.moa.objects.resources.v1_0.AccountAliasQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.AccountQuerySpecification;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import edu.emory.it.services.tki.util.DuoSecurityUtil;
import edu.emory.it.services.tki.util.ErrorUtil;
import edu.emory.moa.jmsobjects.authentication.v1_0.SecurityAssertion;
import edu.emory.moa.objects.resources.v1_0.Credential;
import edu.emory.moa.objects.resources.v1_0.Datetime;
import edu.emory.moa.objects.resources.v1_0.RoleDetail;
import edu.emory.moa.objects.resources.v1_0.SecurityAssertionRequisition;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jsoup.Jsoup;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.moa.objects.resources.Error;
import org.xml.sax.InputSource;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.jms.JMSException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Provider for Security Assertion requests.
 * Handles login ("netid/password") requests and Duo SMS passcodes ("duo") requests and Health checks ("health").
 */
public class AuthenticationSecurityAssertionProvider implements SecurityAssertionProvider {

    private static final Logger logger = LogManager.getLogger("edu.emory.it.services.tki");
    private static final String LOGTAG = "[AuthenticationSecurityAssertionProvider] ";

    /** Perform NetId authentication via SSO login URL. */
    private static final String SECURITY_ASSERTION_TYPE_NETID = "netid/password";
    /** Perform Duo authentication */
    private static final String SECURITY_ASSERTION_TYPE_DUO = "duo";
    /** Perform health check */
    private static final String SECURITY_ASSERTION_HEALTH_CHECK = "health";

    /** Property name for the IdP URL */
    private static final String IDP_AUTH_URL_PROPERTY = "IDP_AUTH_URL";

    private AppConfig appConfig;
    private String idpAuthUrl;
    private Properties duoProperties;
    private ProducerPool awsAccountProducerPool;
    private ProducerPool tkiServiceProducerPool;
    private Cipher cipher; // for decrypting the password

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        if (aConfig == null)
            throw new ProviderException("Must have an AppConfig");

        appConfig = aConfig;

        Properties generalProperties;

        try {
            generalProperties = appConfig.getProperties("GeneralProperties");
            duoProperties = DuoSecurityUtil.getDuoProperties(generalProperties);
        }
        catch (EnterpriseConfigurationObjectException e) {
            throw new ProviderException(e.getMessage());
        }

        idpAuthUrl = generalProperties.getProperty(IDP_AUTH_URL_PROPERTY);
        if (idpAuthUrl == null) {
            throw new ProviderException("GeneralProperties is missing the required property: " + IDP_AUTH_URL_PROPERTY);
        }
        logger.info(LOGTAG + "Using IdP Auth URL: " + idpAuthUrl);

        // used to retrieve account alias information from AWS Account Service
        awsAccountProducerPool = ProviderUtil.getProducerPool("AwsAccountP2pProducer", appConfig, LOGTAG);
        // used to send messages to our own service for persistence
        tkiServiceProducerPool = ProviderUtil.getProducerPool("TkiServiceRequestProducer", appConfig, LOGTAG);

        // credentials for AWS Secrets Manager
        String secretsAccessKeyId = generalProperties.getProperty("secretsAccessKeyId");
        if (secretsAccessKeyId == null) {
            throw new ProviderException("GeneralProperties is missing the required property: secretsAccessKeyId");
        }
        String secretsSecretKey = generalProperties.getProperty("secretsSecretKey");
        if (secretsSecretKey == null) {
            throw new ProviderException("GeneralProperties is missing the required property: secretsSecretKey");
        }
        String secretsPrivateKeyName = generalProperties.getProperty("secretsPrivateKeyName");
        if (secretsPrivateKeyName == null) {
            throw new ProviderException("GeneralProperties is missing the required property: secretsPrivateKeyName");
        }

        try {
            AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(secretsAccessKeyId, secretsSecretKey)))
                    .withRegion("us-east-1")
                    .build();

            GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                    .withSecretId(secretsPrivateKeyName);
            GetSecretValueResult getSecretValueResult = client.getSecretValue(getSecretValueRequest);
            ByteBuffer secretBinaryBytes = getSecretValueResult.getSecretBinary();
            byte[] privateKeyBytes = new byte[secretBinaryBytes.capacity()];
            secretBinaryBytes.get(privateKeyBytes, 0, privateKeyBytes.length);

            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            KeyFactory privateKeyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = privateKeyFactory.generatePrivate(privateKeySpec);

            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException e) {
            throw new ProviderException("Error building private key.  " + e.getMessage());
        }
    }

    @Override
    public SecurityAssertion generate(SecurityAssertionRequisition requisition, List<Error> errors) throws ProviderException {
        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin generate");

        try {
            if (SECURITY_ASSERTION_TYPE_NETID.equalsIgnoreCase(requisition.getType())) {
                return netIdAssertion(requisition, errors);
            }
            else if (SECURITY_ASSERTION_TYPE_DUO.equalsIgnoreCase(requisition.getType())) {
                return duoAssertion(requisition, errors);
            }
            else if (SECURITY_ASSERTION_HEALTH_CHECK.equalsIgnoreCase(requisition.getType())) {
                return healthCheck(requisition, errors);
            }
            else {
                errors.add(ErrorUtil.buildError("application", "TKI-3000",
                        "Unsupported SecurityAssertionRequisition Type: " + requisition.getType()));
                return null;
            }
        }
        finally {
            logger.info(LOGTAG + "Generate complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
        }
    }

    private SecurityAssertion netIdAssertion(SecurityAssertionRequisition requisition, List<Error> errors) throws ProviderException {

        if (!requisition.getCredential().getType().equalsIgnoreCase("password")) {
            errors.add(ErrorUtil.buildError("application", "TKI-3001",
                    "Unsupported " + requisition.getType() + " Credential Type: " + requisition.getCredential().getType()));
            return null;
        }

        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "Begin netIdAssertion");

        // password comes encrypted from the client - decrypt it here
        String principal = requisition.getPrincipal();
        String encryptedPassword = requisition.getCredential().getSecret();
        String password;
        try {
            byte[] encryptedPasswordBytes = Base64.getDecoder().decode(encryptedPassword.getBytes(StandardCharsets.UTF_8));
            password = new String(cipher.doFinal(encryptedPasswordBytes));
        }
        catch (IllegalBlockSizeException | BadPaddingException e) {
            String msg = "Error while decrypting password. Exception: " + e.getMessage();
            logger.error(LOGTAG + msg);
            throw new ProviderException(msg);
        }

        // the SAML response will be scraped from the page returned by a successful login
        String samlResponse = null;
        // parsed HTML
        org.jsoup.nodes.Document loginPageDoc;
        org.jsoup.nodes.Document loginPostResponseDoc;
        String loginPageHtml = null;
        String loginPostResponseHtml = null;

        /*
         * GET the SSO Login page HTML and parse to a jsoup Document
         */
        HttpContext httpContext = new BasicHttpContext();
        httpContext.setAttribute(HttpClientContext.COOKIE_STORE, new BasicCookieStore());

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            logger.info(LOGTAG + "Getting page contents from " + idpAuthUrl);

            HttpGet httpGet = new HttpGet(idpAuthUrl);
            loginPageHtml = httpclient.execute(httpGet, new BasicResponseHandler(), httpContext);

            logger.debug(LOGTAG + "Login page contents: " + loginPageHtml);

            loginPageDoc = Jsoup.parse(loginPageHtml);

            logger.debug(LOGTAG + "Login page contents parsed");
        }
        catch (IOException e) {
            String msg = "Unexpected login page. Exception: " + e.getMessage() + ". Login page contents " + loginPageHtml;
            logger.error(LOGTAG + msg);
            throw new ProviderException(msg);
        }

        /*
         * Scrape the login form to gather form fields and the action URL.
         * These fields, combined with NetID and password from the requisition request, will be POSTed to the action URL.
         */
        boolean usernameFound = false;
        boolean passwordFound = false;
        List<NameValuePair> paramList = new ArrayList<>();
        for (org.jsoup.nodes.Element inputElement : loginPageDoc.getElementsByTag("input")) {
            String key = inputElement.attr("name");
            String value;

            switch (key) {
                case "j_username":
                    value = principal;
                    usernameFound = true;
                    break;
                case "j_password":
                    value = password;
                    passwordFound = true;
                    break;
                default:
                    value = inputElement.attr("value");
                    break;
            }

            paramList.add(new BasicNameValuePair(key, value));
        }
        paramList.add(new BasicNameValuePair("_eventId_proceed", ""));  // the form "Login" button element

        String action = null;
        for (org.jsoup.nodes.Element formElement : loginPageDoc.getElementsByTag("form")) {
            if (formElement.attributes().hasKey("action")) {
                action = formElement.attr("action");
                break;
            }
        }

        // sanity check that the form provided the required fields
        if (!usernameFound) {
            String msg = "Form input field 'j_username' not found. Login page contents: " + loginPageHtml;
            logger.error(LOGTAG + msg);
            throw new ProviderException(msg);
        }
        if (!passwordFound) {
            String msg = "Form input field 'j_password' not found. Login page contents: " + loginPageHtml;
            logger.error(LOGTAG + msg);
            throw new ProviderException(msg);
        }
        if (action == null) {
            String msg = "Form 'action' attribute not found. Login page contents: " + loginPageHtml;
            logger.error(LOGTAG + msg);
            throw new ProviderException(msg);
        }


        // create the URL that we'll POST to in order to perform the login
        URI theAuthUrl = URI.create(idpAuthUrl);
        URI actionUrl = URI.create(action);
        String actionUrlString = String.format("%s://%s%s%s?%s",
                ((actionUrl.getScheme() == null) ? theAuthUrl.getScheme() : actionUrl.getScheme()),
                ((actionUrl.getHost() == null) ? theAuthUrl.getHost() : actionUrl.getHost()),
                ((actionUrl.getPort() == -1) ? ((theAuthUrl.getPort() == -1) ? "" : String.format(":%d", theAuthUrl.getPort()))
                        : String.format(":%d", actionUrl.getPort())),
                actionUrl.getPath(), actionUrl.getQuery());

        /*
         * POST back to perform the actual Login.
         * Then find the SAMLResponse hidden field in the POST response.
         */
        try (CloseableHttpClient httpclient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build()) {
            logger.info(LOGTAG + "Login POST to " + actionUrlString);

            HttpPost httpPost = new HttpPost(actionUrlString);
            httpPost.setEntity(new UrlEncodedFormEntity(paramList));
            loginPostResponseHtml = httpclient.execute(httpPost, new BasicResponseHandler(), httpContext);

            logger.debug(LOGTAG + "Login POST response: " + loginPostResponseHtml);

            loginPostResponseDoc = Jsoup.parse(loginPostResponseHtml);

            logger.debug(LOGTAG + "Login POST response parsed");
        }
        catch (IOException e) {
            String msg = "Unexpected login response. Exception: " + e.getMessage() + ". Login response contents " + loginPostResponseHtml;
            logger.error(LOGTAG + msg);
            throw new ProviderException(msg);
        }

        for (org.jsoup.nodes.Element inputElement : loginPostResponseDoc.getElementsByTag("input")) {
            if (inputElement.attr("name").equals("SAMLResponse")) {
                samlResponse = inputElement.attr("value");
                break;
            }
        }

        /* now that the rewrite rule has been tightened up we have to fake the SAML response
        try { samlResponse = new String(java.nio.file.Files.readAllBytes(java.nio.file.Paths.get("fake-saml-response.b64"))); }
        catch (IOException e) { logger.error(LOGTAG + "Fake SAML exception: " + e.getMessage()); throw new ProviderException(e.getMessage()); }
        */

        /*
         * a variety of errors could cause the SAMLResponse to be null.
         * for example, the user could have sent in an invalid username or password.
         */
        if (samlResponse == null) {
            String errMsg = null;
            String errNumber = null;
            for (org.jsoup.nodes.Element pElement : loginPostResponseDoc.getElementsByTag("p")) {
                if (pElement.text().contains("you entered was incorrect")) {
                    errMsg = pElement.text();
                    errNumber = "TKI-3020";
                    break;
                }
                if (pElement.text().contains("username you entered cannot be identified")) {
                    errMsg = pElement.text();
                    errNumber = "TKI-3021";
                    break;
                }
            }

            if (errMsg == null) {
                // log unexpected errors
                logger.error(LOGTAG + "Login response contents: " + loginPostResponseHtml);
                errMsg = "Unable to login";  // fallback message
                errNumber = "TKI-3002";
            }

            logger.error(LOGTAG + "Login error: " + errMsg);
            errors.add(ErrorUtil.buildError("application", errNumber, errMsg));
            return null;
        }

        /*
         * at this point we've logged in to the network.
         * we still need to get authentication factors from Duo,
         * and decode the SAML assertion to get the roles available to the user.
         */
        SecurityAssertion securityAssertion;

        try {
            securityAssertion = newSecurityAssertionFromRequisition(requisition);

            // Parse the SAML Response and read the roles from it, and write them to our SecurityAssertion response
            logger.debug(LOGTAG + "Finding roles by parsing SAML response " + samlResponse);
            extractRolesFromSAML(samlResponse, securityAssertion);
            annotateRoleDetails(securityAssertion, errors);

            logger.debug(LOGTAG + "Found " + securityAssertion.getRoleDetail().size() + " roles");

            if (duoProperties != null) {
                logger.debug(LOGTAG + "Getting Duo authentication factors");
                DuoSecurityUtil duoSecurityUtil = new DuoSecurityUtil(duoProperties);
                duoSecurityUtil.getAvailableAuthenticationFactors(securityAssertion, errors);
                logger.debug(LOGTAG + "Found " + securityAssertion.getDevice().size() + " Duo authentication factors");
            }
            else {
                logger.debug(LOGTAG + "Duo disabled - not getting authentication factors");
            }
        }
        catch (EnterpriseConfigurationObjectException | EnterpriseFieldException e) {
            String msg = "Error processing SAML response. Exception: " + e.getMessage();
            logger.error(LOGTAG + msg);
            throw new ProviderException(msg);
        }

        /*
         * the SAML response will be set into the SecurityAssertion so that it can be persisted.
         * once persisted, it will be cleared because it can't go back in the response to the client.
         */
        try {
            try {
                securityAssertion.setSamlAssertion(samlResponse);
                saveSamlResponse(securityAssertion);
            }
            finally {
                securityAssertion.setSamlAssertion("");
            }
        }
        catch (EnterpriseFieldException e) {
            String msg = "Error saving SAML response. Exception: " + e.getMessage();
            logger.error(LOGTAG + msg);
            throw new ProviderException(msg);
        }

        logger.info(LOGTAG + "netIdAssertion complete.  Execution time (ms): " + (System.currentTimeMillis() - start));
        return securityAssertion;
    }

    private void extractRolesFromSAML(String samlResponse, SecurityAssertion securityAssertion)
            throws ProviderException, EnterpriseFieldException {

        SAXBuilder sb = new SAXBuilder();
        try {
            // new String(Base64.getDecoder().decode(samlResponse))

            InputStream stream = Base64.getDecoder().wrap(new ByteArrayInputStream(samlResponse.getBytes(StandardCharsets.UTF_8)));
            Document samlResponseDocument = sb.build(new InputSource(stream));
            Element samlXmlRootElement = samlResponseDocument.getRootElement();

            Namespace samlNamespace = Namespace.getNamespace("urn:oasis:names:tc:SAML:2.0:assertion");

            Element assertionSubElement = samlXmlRootElement.getChild("Assertion", samlNamespace);
            if (assertionSubElement != null) {
                Element attributeStatementSubElement = assertionSubElement.getChild("AttributeStatement", samlNamespace);
                if (attributeStatementSubElement != null) {
                    for (Object attributeSubElementObj : attributeStatementSubElement.getChildren("Attribute", samlNamespace)) {
                        Element attributeSubElement = (Element) attributeSubElementObj;
                        if (attributeSubElement.getAttribute("FriendlyName").getValue().equals("Role")) {
                            for (Object attributeValueSubElementObj : attributeSubElement.getChildren("AttributeValue", samlNamespace)) {
                                Element attributeValueSubElement = (Element) attributeValueSubElementObj;

                                // expecting provider and role separated by comma
                                String[] roles = attributeValueSubElement.getText().split(",");
                                if (roles.length != 2) {
                                    throw new ProviderException("Unexpected Role from SAML: " + attributeValueSubElement.getText());
                                }

                                // expect something like
                                // arn:aws:iam::999999999999:saml-provider/Site_Dev_IDP,arn:aws:iam::999999999999:role/rhedcloud/RHEDcloudCentralAdministratorRole
                                // but the order of the provider and role can be either way

                                RoleDetail roleDetail = securityAssertion.newRoleDetail();
                                if (roles[0].contains("saml-provider")) {
                                    roleDetail.setPrincipalArn(roles[0]);
                                    roleDetail.setRoleArn(roles[1]);
                                }
                                else {
                                    roleDetail.setRoleArn(roles[0]);
                                    roleDetail.setPrincipalArn(roles[1]);
                                }
                                securityAssertion.addRoleDetail(roleDetail);
                            }
                        }
                    }
                }
            }
        }
        catch (JDOMException | IOException e) {
            throw new ProviderException(e.getMessage());
        }
    }

    private void annotateRoleDetails(SecurityAssertion securityAssertion, List<Error> errors)
            throws ProviderException, EnterpriseFieldException {

        PointToPointProducer awsAccountProducer = null;

        try {
            try {
                awsAccountProducer = (PointToPointProducer) awsAccountProducerPool.getExclusiveProducer();
            }
            catch (JMSException e) {
                errors.add(ErrorUtil.buildError("application", "TKI-3004", e.getMessage()));
                logger.error(LOGTAG + "Error getting exclusive producer for awsAccountProducerPool: " + e.getMessage());
                return;
            }

            // there are two AccountAlias classes - one from rhedcloud-aws-moa and one from rhedcloud-emory-moa
            // in order to make the usage clear they are both fully qualified

            Map<String, Account> allAccounts = new HashMap<>();

            Account accountQuery = (Account) appConfig.getObject("Provisioning.Account");
            AccountQuerySpecification aqs = (AccountQuerySpecification) appConfig.getObject("Provisioning.AccountQuerySpecification");
            com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountAlias provisioningAccountAliasQuery
                    = (com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountAlias) appConfig.getObject("Provisioning.AccountAlias");
            AccountAliasQuerySpecification aaqs = (AccountAliasQuerySpecification) appConfig.getObject("Provisioning.AccountAliasQuerySpecification");

            logger.info(LOGTAG + "query for all accounts");
            try {
                long queryStart = System.currentTimeMillis();
                @SuppressWarnings("unchecked")
                List<Account> accounts = awsAccountProducer.query(aqs, accountQuery);
                logger.info(LOGTAG + "got " + accounts.size() + " accounts. Execution time (ms): " + (System.currentTimeMillis() - queryStart));
                for (Account account : accounts) {
                    allAccounts.put(account.getAccountId(), account);
                }
            }
            catch (Exception e) {
                errors.add(ErrorUtil.buildError("application", "TKI-3005", e.getMessage()));
                logger.error(LOGTAG + "Error querying all accounts. Exception: " + e.getMessage());
                return;
            }

            // annotate each role
            for (Object rd : securityAssertion.getRoleDetail()) {
                RoleDetail roleDetail = (RoleDetail) rd;
                String[] arnBits = roleDetail.getPrincipalArn().split(":");
                if (arnBits.length != 6) {
                    // expected something like: arn:aws:iam::999999999999:saml-provider/Site_Dev_IDP
                    throw new ProviderException("Unexpected ARN from SAML: " + roleDetail.getPrincipalArn());
                }
                String accountId = arnBits[4];

                // ignore accounts returned in the SAML assertion that are not in our current environment
                if (!allAccounts.containsKey(accountId)) {
                    logger.error(LOGTAG + "Skipping unknown account " + accountId);
                    continue;
                }

                roleDetail.setAccountAlternateName(allAccounts.get(accountId).getAlternateName());

                logger.info(LOGTAG + "account alias query begin for account ID " + accountId);
                try {
                    long queryStart = System.currentTimeMillis();
                    aaqs.setAccountId(accountId);
                    @SuppressWarnings("unchecked")
                    List<com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountAlias> provisioningAccountAliases
                            = awsAccountProducer.query(aaqs, provisioningAccountAliasQuery);
                    logger.info(LOGTAG + "account alias query complete. Execution time (ms): " + (System.currentTimeMillis() - queryStart));
                    if (provisioningAccountAliases.size() > 0) {
                        // if there's more than one, just take the first
                        edu.emory.moa.objects.resources.v1_0.AccountAlias accountAlias = roleDetail.newAccountAlias();
                        accountAlias.setName(provisioningAccountAliases.get(0).getName());
                        accountAlias.setAccountId(accountId);
                        roleDetail.setAccountAlias(accountAlias);
                    }
                }
                catch (Exception e) {
                    logger.error(LOGTAG + "Error during account alias query for account ID " + accountId + ". Exception: " + e.getMessage());
                }
            }

            // Filter out roles that do not have accountAlias
            @SuppressWarnings("unchecked")
            List<RoleDetail> roleDetails = securityAssertion.getRoleDetail();
            securityAssertion.setRoleDetail(roleDetails.stream()
                    .filter(roleDetail -> roleDetail.getAccountAlias() != null)
                    .collect(Collectors.toList()));
        }
        catch (EnterpriseConfigurationObjectException e) {
            throw new ProviderException(e.getMessage());
        }
        finally {
            if (awsAccountProducer != null)
                awsAccountProducerPool.releaseProducer(awsAccountProducer);
        }
    }

    private void saveSamlResponse(SecurityAssertion securityAssertion)
            throws ProviderException {

        PointToPointProducer tkiServiceProducer = null;

        long createStart = System.currentTimeMillis();
        logger.info(LOGTAG + "security assertion create begin");
        try {
            tkiServiceProducer = (PointToPointProducer) tkiServiceProducerPool.getExclusiveProducer();
            tkiServiceProducer.setRequestTimeoutInterval(5 * 60 * 1000); // 5 minutes

            securityAssertion.setCommandName("RdbmsRequestCommand");

            securityAssertion.create(tkiServiceProducer);
        }
        catch (JMSException | EnterpriseObjectCreateException e) {
            throw new ProviderException(e.getMessage());
        }
        finally {
            if (tkiServiceProducer != null)
                tkiServiceProducerPool.releaseProducer(tkiServiceProducer);

            logger.info(LOGTAG + "security assertion create complete. Execution time (ms): " + (System.currentTimeMillis() - createStart));
        }
    }

    private SecurityAssertion duoAssertion(SecurityAssertionRequisition requisition, List<Error> errors) throws ProviderException {
        try {
            if (duoProperties == null) {
                logger.debug(LOGTAG + "Duo disabled - not performing sms auth");
                return newSecurityAssertionFromRequisition(requisition);
            }
        }
        catch (EnterpriseConfigurationObjectException | EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }

        DuoSecurityUtil duoSecurityUtil = new DuoSecurityUtil(duoProperties);

        String username = requisition.getPrincipal();
        String device;

        /*
         * Credential Type (Duo factor) is mandatory and can be:
         *
         * sms
         *   Send a new batch of SMS passcodes to the user.
         *   the Credential Id will be the device ID
         *   if Id is omitted, the first of the user's devices with the "sms" capability will be used
         *
         * others
         *   not supported here - see AwsRoleAssumptionProvider
         */
        Credential credential = requisition.getCredential();
        String credentialType = credential.getType();
        switch (credentialType) {
            case "sms":
                device = credential.getId();
                if (device == null) {
                    // "auto" to use the first of the user's devices with the "sms" capability
                    device = "auto";
                }
                break;
            default:
                errors.add(ErrorUtil.buildError("application", "TKI-3003", "Unsupported Duo Authentication method: " + credentialType));
                return null;
        }

        try {
            duoSecurityUtil.sms(username, device, errors);
            // return a basic response
            return newSecurityAssertionFromRequisition(requisition);
        }
        catch (EnterpriseConfigurationObjectException | EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }
    }

    private SecurityAssertion healthCheck(SecurityAssertionRequisition requisition, List<Error> errors) throws ProviderException {
        try {
            logger.info(LOGTAG + "running health check");
            // return a basic response
            return newSecurityAssertionFromRequisition(requisition);
        }
        catch (EnterpriseConfigurationObjectException | EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }
    }

    /**
     * Create a new SecurityAssertion and fill in required fields from the requisition.
     *
     * @param requisition SecurityAssertionRequisition
     * @return SecurityAssertion
     * @throws EnterpriseFieldException on error
     * @throws EnterpriseConfigurationObjectException on error
     */
    private SecurityAssertion newSecurityAssertionFromRequisition(SecurityAssertionRequisition requisition)
            throws EnterpriseFieldException, EnterpriseConfigurationObjectException {

        SecurityAssertion securityAssertion;
        securityAssertion = (SecurityAssertion) appConfig.getObjectByType(SecurityAssertion.class.getName());

        securityAssertion.setType(requisition.getType());
        securityAssertion.setPrincipal(requisition.getPrincipal());
        Datetime datetime = securityAssertion.newCreateDatetime();
        datetime.update(Calendar.getInstance());
        securityAssertion.setCreateDatetime(datetime);

        return securityAssertion;
    }
}
