# The RHEDcloud Temporary Key Issuance Service

##Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

##License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

##Description
The RHEDcloud Temporary Key Issuance (TKI) service provides mechanisms for users to authenticate to Amazon Web Services using IdM credentials and optional Two-Factor Authentication (TFA).

## Configuration
Please see the [configuration guide](README-configuration.md) for details.

## Message Flow
The service implements two commands: `SecurityAssertion` and `RoleAssumption`.
It takes a combination of both commands to complete the issuance of temporary keys.

The first step is to authenticate with the IdM login and possibly gather device information for TFA.
In these examples, the TFA will use [Duo Security](https://duo.com)

```xml
    <SecurityAssertionRequisition>
        <Type>netid/password</Type>
        <Principal>jenny</Principal>
        <Credential>
            <Type>password</Type>
            <Secret>who can i turn to</Secret>
        </Credential>
    </SecurityAssertionRequisition>
```

The response will include Duo devices as well as the roles that are available in AWS.
Because it can be difficult to remember AWS account IDs, `<AccountAlias>` contains the account alias assigned to the account.

```xml
    <SecurityAssertion>
        <Type>netid/password</Type>
        <Principal>jenny</Principal>
        <Device>
            <DeviceId>DPQ9DEIXZELI9YN9Q9JX</DeviceId>
            <Capability>auto</Capability>
            <Capability>phone</Capability>
            <DisplayName>Landline (XXX-867-5309)</DisplayName>
            <Number>615-867-5309</Number>
            <Type>phone</Type>
        </Device>
        <Device>
            <DeviceId>DP9XX99KAJCKBMWKI9H9</DeviceId>
            <Capability>auto</Capability>
            <Capability>push</Capability>
            <Capability>sms</Capability>
            <Capability>phone</Capability>
            <Capability>mobile_otp</Capability>
            <DisplayName>Android (XXX-867-5309)</DisplayName>
            <Number>865-867-5309</Number>
            <Type>phone</Type>
        </Device>
        <RoleDetail>
            <AccountAlias>
                <AccountId>123456789012</AccountId>
                <Name>aws-test-1</Name>
            </AccountAlias>
            <PrincipalArn>arn:aws:iam::123456789012:saml-provider/Emory_IDP</PrincipalArn>
            <RoleArn>arn:aws:iam::123456789012:role/rhedcloud/RHEDcloudCentralAdministratorRole</RoleArn>
        </RoleDetail>
        <RoleDetail>
            <AccountAlias>
                <AccountId>123456789012</AccountId>
                <Name>aws-test-1</Name>
            </AccountAlias>
            <PrincipalArn>arn:aws:iam::123456789012:saml-provider/Emory_IDP</PrincipalArn>
            <RoleArn>arn:aws:iam::123456789012:role/rhedcloud/RHEDcloudAuditorRole</RoleArn>
        </RoleDetail>
        <CreateDatetime>
            <Year>1999</Year>
            <Month>12</Month>
            <Day>31</Day>
            <Hour>11</Hour>
            <Minute>59</Minute>
            <Second>59</Second>
            <SubSecond>999</SubSecond>
            <Timezone>America/Denver</Timezone>
        </CreateDatetime>
    </SecurityAssertion>
```

The next step depends on how `jenny` would like to interact with Duo.
If `jenny` would like to issue new SMS codes then another `SecurityAssertion` is needed:

```xml
    <SecurityAssertionRequisition>
        <Type>duo</Type>
        <Principal>jenny</Principal>
        <Credential>
            <Type>sms</Type>
             <!-- optional DeviceId that supports the 'sms' capability -->
            <Id>DP9XX99KAJCKBMWKI9H9</Id>
        </Credential>
    </SecurityAssertionRequisition>
```

The response is not important (and contains no actionable information) as long as it is not an error.

Duo will interact directly with the user (`jenny`) by sending new SMS codes which will be used in the next message.

The final step in the issuance of temporary keys is the assumption of the desired role.

```xml
    <RoleAssumptionRequisition>
        <Principal>jenny</Principal>
        <Region>us-east-1</Region>
        <RoleDetail>
          <AccountAlias>  <!-- not used but must be given -->
              <AccountId>123456789012</AccountId>
              <Name>aws-test-1</Name>
          </AccountAlias>
          <PrincipalArn>arn:aws:iam::123456789012:saml-provider/Emory_IDP</PrincipalArn>
          <RoleArn>arn:aws:iam::123456789012:role/rhedcloud/RHEDcloudAuditorRole</RoleArn>
        </RoleDetail>
        <Credential>
            <Type>push</Type>
            <Id>optional DeviceId</Id>
        </Credential>
        <DurationSeconds>43200</DurationSeconds>
    </RoleAssumptionRequisition>
```

The `<PrincipalArn>` and `<RoleArn>` come directly from the `<SecurityAssertion>` response.

The `<DurationSeconds>` element defines the length of the temporary session.
The maximum is 12 hours but may be shorter depending on a setting in the AWS role.

When the `<Credential>` `<Type>` is `push` (or anything other than `passcode`) then Duo will authorize directly
with `jenny` before assuming the role in AWS.
If SMS codes were issued in the previous step then the `<Credential>` section will look like this:

```xml
    <Credential>
        <Type>passcode</Type>
        <Id>optional DeviceId</Id>
        <!-- this is the code that was sent directly to jenny -->
        <Secret>1234567</Secret>
    </Credential>
```

If the Duo authentication and AWS role assumption are successful the temporary keys will be included in the response.

```xml
    <RoleAssumption>
        <Audience>https://signin.aws.amazon.com/saml</Audience>
        <Issuer>https://login.emory.edu/idp/shibboleth</Issuer>
        <NameQualifier>9zPh9/9L9X9qrfeGdl9zde9xhE9=</NameQualifier>
        <Subject>_9a9b999a99e999999e99fcc99be9ab99</Subject>
        <SubjectType>transient</SubjectType>
        <AssumedRoleUser>
            <Arn>arn:aws:sts::123456789012:assumed-role/Administrator/P9999999</Arn>
            <AssumedRoleId>AROAIX3IMMANUCZIKILAI:P9999999</AssumedRoleId>
        </AssumedRoleUser>
        <Credentials>
            <AccessKeyId>ASIAJ263LHWU4B</AccessKeyId>
            <Expiration>Wed Apr 11 20:09:36 CDT 2018</Expiration>
            <SecretAccessKey>MzyoamQ0wNU6e56g5H1qUZ</SecretAccessKey>
            <SessionToken>FQoDYXdzECIaDNWHhNR57==</SessionToken>
        </Credentials>
        <Token>RElFMVhFUzUxT9NSWEdBS99=||99999|9b14b9999e91e9d</Token> <!-- Duo trusted_device_token -->
    </RoleAssumption>
```

The `<AccessKeyId>`, `<SecretAccessKey>`, and `<SessionToken>` can be put in the `$HOME/.aws/credentials` file as
either the primary entry or as a new profile.  For example,

```
[tki]
aws_access_key_id = ASIAJ263LHWU4B
aws_secret_access_key = MzyoamQ0wNU6e56g5H1qUZ
aws_session_token = FQoDYXdzECIaDNWHhNR57==
```

And then the AWS command line client can be used:

`$ aws rds describe-db-clusters --profile tki`

## Errors
Other than unexpected error conditions, there are a few more common errors that can occur.

For example, if the wrong `netid` is used, the error will look something like this:

```xml
    <Result action="Generate" status="failure">
      <ProcessedMessageId>...</ProcessedMessageId>
      <Error type="application">
        <ErrorNumber>TKI-3021</ErrorNumber>
        <ErrorDescription>The username you entered cannot be identified.</ErrorDescription>
      </Error>
    </Result>
```

If the wrong password is used, the error will look something like this:

```xml
    <Result action="Generate" status="failure">
      <ProcessedMessageId>...</ProcessedMessageId>
      <Error type="application">
        <ErrorNumber>TKI-3020</ErrorNumber>
        <ErrorDescription>The password you entered was incorrect.</ErrorDescription>
      </Error>
    </Result>
```

The length of time between a successful login using a `SecurityAssertionRequisition` message and the `RoleAssumptionRequisition` message is limited. If it is exceeded the error will look something like this:

```xml
    <Result action="Generate" status="failure">
      <ProcessedMessageId>...</ProcessedMessageId>
      <Error type="application">
        <ErrorNumber>TKI-4010</ErrorNumber>
        <ErrorDescription>Token must be redeemed within 5 minutes of issuance</ErrorDescription>
      </Error>
    </Result>
```

As noted above, `<DurationSeconds>` has a maximum value and if a request is made with a value exceeding the maximum the error will look something like this:

```xml
    <Result action="Generate" status="failure">
      <ProcessedMessageId>...</ProcessedMessageId>
      <Error type="application">
        <ErrorNumber>TKI-4011</ErrorNumber>
        <ErrorDescription>DurationSeconds must have value less than or equal to 43200</ErrorDescription>
      </Error>
    </Result>
```

If the duration is limited by the setting in the AWS role, the error will look something like this:

```xml
    <Result action="Generate" status="failure">
      <ProcessedMessageId>...</ProcessedMessageId>
      <Error type="application">
        <ErrorNumber>TKI-4012</ErrorNumber>
        <ErrorDescription>The requested DurationSeconds exceeds the MaxSessionDuration set for this role.</ErrorDescription>
      </Error>
    </Result>
```

Finally, there are Duo related errors but a few in particular will be described here.
When Duo prompts `jenny` with a Login Request, it can timeout or it can be explicitly denied.
If it times out, the error will look something like this:

```xml
    <Result action="Generate" status="failure">
      <ProcessedMessageId>...</ProcessedMessageId>
      <Error type="application">
        <ErrorNumber>TKI-5003</ErrorNumber>
        <ErrorDescription>Duo authorization denied for user kboyes: Login timed out.</ErrorDescription>
      </Error>
    </Result>
```

And like this if the Login Request was denied:

```xml
    <Result action="Generate" status="failure">
      <ProcessedMessageId>...</ProcessedMessageId>
      <Error type="application">
        <ErrorNumber>TKI-5003</ErrorNumber>
        <ErrorDescription>Duo authorization denied for user kboyes: Login request denied.</ErrorDescription>
      </Error>
    </Result>
```

If using SMS codes and the wrong passcode is sent, the error will look something like this:

```xml
    <Result action="Generate" status="failure">
      <ProcessedMessageId>...</ProcessedMessageId>
      <Error type="application">
        <ErrorNumber>TKI-5003</ErrorNumber>
        <ErrorDescription>Duo authorization denied for user kboyes: Incorrect passcode. Please try again.</ErrorDescription>
      </Error>
    </Result>
```

Except you can not try again without initiating the entire process again starting with the `SecurityAssertionRequisition`.
